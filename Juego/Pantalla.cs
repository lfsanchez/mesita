﻿using OpenTK.Windowing.Common;
using OpenTK.Windowing.Desktop;
using OpenTK.Windowing.GraphicsLibraryFramework;
using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Input;
using OpenTK.Graphics.OpenGL;
using System.Linq;
using OpenTK.Mathematics;

namespace Juego
{
    class Pantalla : GameWindow
    {
        float[] vertices =
        {
            //base superior
            -0.5f, 0.5f, -1, 0.0f, 1.0f,
            -0.5f, 0.5f,  0, 0.0f, 0.0f, 
             0.5f, 0.5f,  0, 1.0f, 0.0f,

             0.5f, 0.5f,  0, 1.0f, 0.0f,
            -0.5f, 0.5f, -1, 0.0f, 1.0f,
             0.5f, 0.5f, -1, 1.0f, 1.0f,
            //base inferior
            -0.5f, 0.45f, -1, 0.0f, 1.0f,
            -0.5f, 0.45f,  0, 0.0f, 0.0f,
             0.5f, 0.45f,  0, 1.0f, 0.0f,

             0.5f, 0.45f,  0, 1.0f, 0.0f,
            -0.5f, 0.45f, -1, 0.0f, 1.0f,
             0.5f, 0.45f, -1, 1.0f, 1.0f,
            //borde frontal
            -0.5f, 0.5f, 0, 0.0f, 1.0f,
            -0.5f, 0.45f, 0, 0.0f, 0.0f,
             0.5f, 0.5f, 0, 1.0f, 1.0f,

            -0.5f, 0.45f, 0, 0.0f, 0.0f,
             0.5f, 0.5f, 0, 1.0f, 1.0f,
             0.5f, 0.45f, 0, 1.0f, 0.0f,
            //borde trasero
            -0.5f, 0.5f, -1, 0.0f, 1.0f,
            -0.5f, 0.45f, -1, 0.0f, 0.0f,
             0.5f, 0.5f, -1, 1.0f, 1.0f,

            -0.5f, 0.45f, -1, 0.0f, 0.0f,
             0.5f, 0.5f, -1, 1.0f, 1.0f,
             0.5f, 0.45f, -1, 1.0f, 0.0f,
            //borde lateral izquierdo
            -0.5f, 0.5f, 0, 0.0f, 1.0f,
            -0.5f, 0.45f, 0, 0.0f, 0.0f,
            -0.5f, 0.5f, -1, 1.0f, 1.0f,
                
            -0.5f, 0.5f, -1, 1.0f, 1.0f,
            -0.5f, 0.45f, 0, 0.0f, 0.0f,
            -0.5f, 0.45f, -1, 1.0f, 0.0f,
            //borde lateral derecho
             0.5f, 0.5f, 0, 0.0f, 1.0f,
             0.5f, 0.45f, 0, 0.0f, 0.0f,
             0.5f, 0.5f, -1, 1.0f, 1.0f,

             0.5f, 0.5f, -1, 1.0f, 1.0f,
             0.5f, 0.45f, 0, 0.0f, 0.0f,
             0.5f, 0.45f, -1, 1.0f, 0.0f,
             //pata 1
             //base superior
            -0.4f, 0.45f, -0.2f, 0.0f, 1.0f,
            -0.4f, 0.45f,  -0.1f, 0.0f, 0.0f,
            -0.3f, 0.45f,  -0.1f, 1.0f, 0.0f,

            -0.3f, 0.45f,  -0.1f, 1.0f, 0.0f,
            -0.4f, 0.45f, -0.2f, 0.0f, 1.0f,
            -0.3f, 0.45f, -0.2f, 1.0f, 1.0f,
            //base inferior
            -0.4f, -0.1f, -0.2f, 0.0f, 1.0f,
            -0.4f, -0.1f,  -0.1f, 0.0f, 0.0f,
            -0.3f, -0.1f,  -0.1f, 1.0f, 0.0f,

            -0.3f, -0.1f,  -0.1f, 1.0f, 0.0f,
            -0.4f, -0.1f, -0.2f, 0.0f, 1.0f,
            -0.3f, -0.1f, -0.2f, 1.0f, 1.0f,
            //borde frontal
            -0.4f, 0.45f, -0.1f, 0.0f, 1.0f,
            -0.4f, -0.1f, -0.1f, 0.0f, 0.0f,
            -0.3f, 0.45f, -0.1f, 1.0f, 1.0f,

            -0.4f, -0.1f, -0.1f, 0.0f, 0.0f,
            -0.3f, 0.45f, -0.1f, 1.0f, 1.0f,
            -0.3f, -0.1f, -0.1f, 1.0f, 0.0f,
            //borde trasero
            -0.4f, 0.45f, -0.2f, 0.0f, 1.0f,
            -0.4f, -0.1f, -0.2f, 0.0f, 0.0f,
            -0.3f, 0.45f, -0.2f, 1.0f, 1.0f,

            -0.4f, -0.1f, -0.2f, 0.0f, 0.0f,
            -0.3f, 0.45f, -0.2f, 1.0f, 1.0f,
            -0.3f, -0.1f, -0.2f, 1.0f, 0.0f,
            //borde lateral izquierdo
            -0.4f, 0.45f, -0.1f, 0.0f, 1.0f,
            -0.4f, -0.1f, -0.1f, 0.0f, 0.0f,
            -0.4f, 0.45f, -0.2f, 1.0f, 1.0f,

            -0.4f, 0.45f, -0.2f, 1.0f, 1.0f,
            -0.4f, -0.1f, -0.1f, 0.0f, 0.0f,
            -0.4f, -0.1f, -0.2f, 1.0f, 0.0f,
            //borde lateral derecho
            -0.3f, 0.45f, -0.1f, 0.0f, 1.0f,
            -0.3f, -0.1f, -0.1f, 0.0f, 0.0f,
            -0.3f, 0.45f, -0.2f, 1.0f, 1.0f,

            -0.3f, 0.45f, -0.2f, 1.0f, 1.0f,
            -0.3f, -0.1f, -0.1f, 0.0f, 0.0f,
            -0.3f, -0.1f, -0.2f, 1.0f, 0.0f,
            //pata 2
             //base superior
            -0.4f, 0.45f, -0.8f, 0.0f, 1.0f,
            -0.4f, 0.45f,  -0.9f, 0.0f, 0.0f,
            -0.3f, 0.45f,  -0.9f, 1.0f, 0.0f,

            -0.3f, 0.45f,  -0.9f, 1.0f, 0.0f,
            -0.4f, 0.45f, -0.8f, 0.0f, 1.0f,
            -0.3f, 0.45f, -0.8f, 1.0f, 1.0f,
            //base inferior
            -0.4f, -0.1f, -0.8f, 0.0f, 1.0f,
            -0.4f, -0.1f,  -0.9f, 0.0f, 0.0f,
            -0.3f, -0.1f,  -0.9f, 1.0f, 0.0f,

            -0.3f, -0.1f,  -0.9f, 1.0f, 0.0f,
            -0.4f, -0.1f, -0.8f, 0.0f, 1.0f,
            -0.3f, -0.1f, -0.8f, 1.0f, 1.0f,
            //borde frontal
            -0.4f, 0.45f, -0.9f, 0.0f, 1.0f,
            -0.4f, -0.1f, -0.9f, 0.0f, 0.0f,
            -0.3f, 0.45f, -0.9f, 1.0f, 1.0f,

            -0.4f, -0.1f, -0.9f, 0.0f, 0.0f,
            -0.3f, 0.45f, -0.9f, 1.0f, 1.0f,
            -0.3f, -0.1f, -0.9f, 1.0f, 0.0f,
            //borde trasero
            -0.4f, 0.45f, -0.8f, 0.0f, 1.0f,
            -0.4f, -0.1f, -0.8f, 0.0f, 0.0f,
            -0.3f, 0.45f, -0.8f, 1.0f, 1.0f,

            -0.4f, -0.1f, -0.8f, 0.0f, 0.0f,
            -0.3f, 0.45f, -0.8f, 1.0f, 1.0f,
            -0.3f, -0.1f, -0.8f, 1.0f, 0.0f,
            //borde lateral izquierdo
            -0.4f, 0.45f, -0.9f, 0.0f, 1.0f,
            -0.4f, -0.1f, -0.9f, 0.0f, 0.0f,
            -0.4f, 0.45f, -0.8f, 1.0f, 1.0f,

            -0.4f, 0.45f, -0.8f, 1.0f, 1.0f,
            -0.4f, -0.1f, -0.9f, 0.0f, 0.0f,
            -0.4f, -0.1f, -0.8f, 1.0f, 0.0f,
            //borde lateral derecho
            -0.3f, 0.45f, -0.9f, 0.0f, 1.0f,
            -0.3f, -0.1f, -0.9f, 0.0f, 0.0f,
            -0.3f, 0.45f, -0.8f, 1.0f, 1.0f,

            -0.3f, 0.45f, -0.8f, 1.0f, 1.0f,
            -0.3f, -0.1f, -0.9f, 0.0f, 0.0f,
            -0.3f, -0.1f, -0.8f, 1.0f, 0.0f,
            //pata 3
             //base superior
             0.3f, 0.45f, -0.2f, 0.0f, 1.0f,
             0.3f, 0.45f,  -0.1f, 0.0f, 0.0f,
             0.4f, 0.45f,  -0.1f, 1.0f, 0.0f,

             0.4f, 0.45f,  -0.1f, 1.0f, 0.0f,
             0.3f, 0.45f, -0.2f, 0.0f, 1.0f,
             0.4f, 0.45f, -0.2f, 1.0f, 1.0f,
            //base inferior
             0.3f, -0.1f, -0.2f, 0.0f, 1.0f,
             0.3f, -0.1f,  -0.1f, 0.0f, 0.0f,
             0.4f, -0.1f,  -0.1f, 1.0f, 0.0f,

             0.4f, -0.1f,  -0.1f, 1.0f, 0.0f,
             0.3f, -0.1f, -0.2f, 0.0f, 1.0f,
             0.4f, -0.1f, -0.2f, 1.0f, 1.0f,
            //borde frontal
             0.3f, 0.45f, -0.1f, 0.0f, 1.0f,
             0.3f, -0.1f, -0.1f, 0.0f, 0.0f,
             0.4f, 0.45f, -0.1f, 1.0f, 1.0f,

             0.3f, -0.1f, -0.1f, 0.0f, 0.0f,
             0.4f, 0.45f, -0.1f, 1.0f, 1.0f,
             0.4f, -0.1f, -0.1f, 1.0f, 0.0f,
            //borde trasero
             0.3f, 0.45f, -0.2f, 0.0f, 1.0f,
             0.3f, -0.1f, -0.2f, 0.0f, 0.0f,
             0.4f, 0.45f, -0.2f, 1.0f, 1.0f,

             0.3f, -0.1f, -0.2f, 0.0f, 0.0f,
             0.4f, 0.45f, -0.2f, 1.0f, 1.0f,
             0.4f, -0.1f, -0.2f, 1.0f, 0.0f,
            //borde lateral izquierdo
             0.3f, 0.45f, -0.1f, 0.0f, 1.0f,
             0.3f, -0.1f, -0.1f, 0.0f, 0.0f,
             0.3f, 0.45f, -0.2f, 1.0f, 1.0f,

             0.3f, 0.45f, -0.2f, 1.0f, 1.0f,
             0.3f, -0.1f, -0.1f, 0.0f, 0.0f,
             0.3f, -0.1f, -0.2f, 1.0f, 0.0f,
            //borde lateral derecho
             0.4f, 0.45f, -0.1f, 0.0f, 1.0f,
             0.4f, -0.1f, -0.1f, 0.0f, 0.0f,
             0.4f, 0.45f, -0.2f, 1.0f, 1.0f,

             0.4f, 0.45f, -0.2f, 1.0f, 1.0f,
             0.4f, -0.1f, -0.1f, 0.0f, 0.0f,
             0.4f, -0.1f, -0.2f, 1.0f, 0.0f,
            //pata 4
             //base superior
             0.3f, 0.45f, -0.8f, 0.0f, 1.0f,
             0.3f, 0.45f,  -0.9f, 0.0f, 0.0f,
             0.4f, 0.45f,  -0.9f, 1.0f, 0.0f,

             0.4f, 0.45f,  -0.9f, 1.0f, 0.0f,
             0.3f, 0.45f, -0.8f, 0.0f, 1.0f,
             0.4f, 0.45f, -0.8f, 1.0f, 1.0f,
            //base inferior
             0.3f, -0.1f, -0.8f, 0.0f, 1.0f,
             0.3f, -0.1f,  -0.9f, 0.0f, 0.0f,
             0.4f, -0.1f,  -0.9f, 1.0f, 0.0f,

             0.4f, -0.1f,  -0.9f, 1.0f, 0.0f,
             0.3f, -0.1f, -0.8f, 0.0f, 1.0f,
             0.4f, -0.1f, -0.8f, 1.0f, 1.0f,
            //borde frontal
             0.3f, 0.45f, -0.9f, 0.0f, 1.0f,
             0.3f, -0.1f, -0.9f, 0.0f, 0.0f,
             0.4f, 0.45f, -0.9f, 1.0f, 1.0f,
             
             0.3f, -0.1f, -0.9f, 0.0f, 0.0f,
             0.4f, 0.45f, -0.9f, 1.0f, 1.0f,
             0.4f, -0.1f, -0.9f, 1.0f, 0.0f,
            //borde trasero
             0.3f, 0.45f, -0.8f, 0.0f, 1.0f,
             0.3f, -0.1f, -0.8f, 0.0f, 0.0f,
             0.4f, 0.45f, -0.8f, 1.0f, 1.0f,
             
             0.3f, -0.1f, -0.8f, 0.0f, 0.0f,
             0.4f, 0.45f, -0.8f, 1.0f, 1.0f,
             0.4f, -0.1f, -0.8f, 1.0f, 0.0f,
            //borde lateral izquierdo
             0.3f, 0.45f, -0.9f, 0.0f, 1.0f,
             0.3f, -0.1f, -0.9f, 0.0f, 0.0f,
             0.3f, 0.45f, -0.8f, 1.0f, 1.0f,

             0.3f, 0.45f, -0.8f, 1.0f, 1.0f,
             0.3f, -0.1f, -0.9f, 0.0f, 0.0f,
             0.3f, -0.1f, -0.8f, 1.0f, 0.0f,
            //borde lateral derecho
             0.4f, 0.45f, -0.9f, 0.0f, 1.0f,
             0.4f, -0.1f, -0.9f, 0.0f, 0.0f,
             0.4f, 0.45f, -0.8f, 1.0f, 1.0f,

             0.4f, 0.45f, -0.8f, 1.0f, 1.0f,
             0.4f, -0.1f, -0.9f, 0.0f, 0.0f,
             0.4f, -0.1f, -0.8f, 1.0f, 0.0f,
        };
        private double time;

        int VertexBufferObject;
        int VertexArrayObject;
        Shader shader;
        Texturas textura;
        public Pantalla(GameWindowSettings config, NativeWindowSettings nativo) : base(config, nativo) { }
        protected override void OnLoad()
        {
            base.OnLoad();
            shader = new Shader();
            textura = new Texturas();
            GL.Enable(EnableCap.DepthTest);
            VertexArrayObject = GL.GenVertexArray();
            GL.BindVertexArray(VertexArrayObject);
            VertexBufferObject = GL.GenBuffer();
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.MirroredRepeat);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.MirroredRepeat);
            float[] borderColor = { 0.0f, 0.0f, 0.0f, 0.0f };
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureBorderColor, borderColor);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.BindBuffer(BufferTarget.ArrayBuffer, VertexBufferObject);
            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StaticDraw);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, true, 5 * sizeof(float), 0);
            int texCoordLocation = shader.GetAttribLocation("aTexCoord");
            GL.EnableVertexAttribArray(texCoordLocation);
            GL.VertexAttribPointer(texCoordLocation, 2, VertexAttribPointerType.Float, true, 5 * sizeof(float), 3 * sizeof(float));
            GL.EnableVertexAttribArray(0);
            shader.Use();
        }
        protected override void OnUnload()
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.DeleteBuffer(VertexBufferObject);
            shader.Dispose();
            base.OnUnload();
        }
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
            time += 5.0 * e.Time;
            Matrix4 view = Matrix4.CreateTranslation(0.0f, -0.5f, -2f);
            Matrix4 projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(45.0f), Size.X / Size.Y, 0.1f, 100.0f);
            Matrix4 modelX = Matrix4.CreateRotationX((float)MathHelper.DegreesToRadians(5));
            Matrix4 modelY = Matrix4.Identity * Matrix4.CreateRotationY((float)MathHelper.DegreesToRadians(time));
            shader.SetMatrix4("modelX", modelX);
            shader.SetMatrix4("modelY", modelY);
            shader.SetMatrix4("view", view);
            shader.SetMatrix4("projection", projection);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.ClearColor(0.5f, 0.0f, 0.5f, 1.0f);
            GL.BindVertexArray(VertexArrayObject);
            GL.DrawArrays(PrimitiveType.Triangles, 0, vertices.Length);

            Context.SwapBuffers();
        }
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            KeyboardState input = KeyboardState;

            if (input.IsKeyDown(Keys.Escape))
            {
                this.Close();
            }
            base.OnUpdateFrame(e);
        }
    }
}
